import * as PIXI from 'pixi.js';
import SlotItem from './SlotItem';

export default class SlotColumn extends PIXI.Container {
    public items: Array<SlotItem>;
    constructor(items?: Array<SlotItem>) {
        super();
        this.addItems();;
    }

    addItems() {
        this.children = [];
        for (let i = 0; i < 40; i++) {
            let number = (Math.floor(Math.random() * 9) + 1).toString();
            let prevItem = this.children[i - 1];
            let positionY = (i === 0) ? 0 : prevItem.y + prevItem.radius * 2 + 20;
            var circle = new SlotItem(0, positionY, 90, number.toString());
            circle.visible = false;
            this.addChild(circle);
        }
    }

    checkVisible() {
        for (let i = 0; i < this.children.length; i++) {
            let currentChild = this.children[i];
            if (!(currentChild.y - (37 * 200 - 200) < 0)) {
                currentChild.visible = true;
            } else {
                currentChild.visible = false;
            }
        }
    }

    checkWinning () {
        for (let i = 0; i < this.children.length; i++) {
            this.children[i].isWinning(this.y);
        }
    }

    onSpin(delay?: number) {
        if (delay) {
            setTimeout(()=>{this.spin()} , delay);
        } else {
            this.spin();
        }
    }

    spin() {
        for (let i = 0; i < this.children.length; i++) {
            let circle = this.children[i];
            this.checkVisible();
            circle.y += 30;
        }
    }

}
