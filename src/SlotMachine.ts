import * as PIXI from 'pixi.js';
import Renderer from './Renderer';
import SlotColumn from './SlotColumn';

export default class SlotMachine extends PIXI.Container {
    public slotColumns: Array<SlotColumn>;
    public spin: boolean;
    constructor(slotColumns: Array<SlotColumn>) {
        super();
        this.spin = false;
        this.slotColumns = slotColumns;
        this.addChildren();
    }

    addChildren() {
        for (let i = 0; i < this.slotColumns.length; i++) {
            this.addChild(this.slotColumns[i]);
            this.adjustComumnPositions(i);
            this.slotColumns[i].checkVisible();
        }
    }

    onSpin () {
        this.spin = true;
        for(let i = 0; i<this.children.length;i++){
            this.children[i].addItems();
        }
    }

    checkWining () {
        for (let i = 0; i < this.children.length; i++) {
            this.children[i].checkWinning();
        }
    }

    stopSpin () {
        this.spin = false;
    }

    spinSlots () {
        let delay = 0;
        for (let i = 0; i < this.slotColumns.length;i++) {
            this.slotColumns[i].onSpin(delay);
            delay+=300;
        }   
    }

    adjustComumnPositions(i: number) {
        let currentItem = this.children[i];
        let prevItem = this.children[i - 1];
        this.children[i].x = (i === 0) ? 0 : prevItem.x + prevItem.width + 15;
        this.children[i].y = - 37 * 200;
    }

    start(renderer: Renderer) {
        renderer.renderStage();
    }
}