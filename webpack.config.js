module.exports = {
    watch: true,
    entry: "./src/app.ts",
    output: {
        path: './bundle',
        filename: "bundle.js"
    },
    resolve: {
        extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: 'ts-loader' }
        ]
    },
    externals: [
        //set pixi as external util fixed by PIXI
        { "pixi.js": "PIXI" }
    ]
};