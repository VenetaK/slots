import * as PIXI from 'pixi.js';
import SlotMachine from './SlotMachine';
import SlotColumn from './SlotColumn';
import SlotItem from './SlotItem';
import Renderer from './Renderer';

//------------------------------
let canvas = <HTMLCanvasElement>document.getElementById('canvas');
let stage = PIXI.autoDetectRenderer(canvas.width, canvas.height, { view: canvas, antialias: true });

let renderer = new Renderer(canvas, stage);

let slotColumn = new SlotColumn();
let slotColumn2 = new SlotColumn();
let slotColumn3 = new SlotColumn();
let slotColumn4 = new SlotColumn();

let app = new SlotMachine([slotColumn, slotColumn2, slotColumn3, slotColumn4]);
let button = <HTMLButtonElement>document.getElementById('spin');
button.onclick = app.onSpin.bind(app);

app.start(renderer);
var counter = 0;
// run the render loop
animate();
function animate() {
    renderer.renderApp(app);
    requestAnimationFrame(animate);
    
    if (app.spin) {
        button.disabled = true;
        counter+=1;
        app.spinSlots();
        if (counter < 100) return;
        app.stopSpin();
        counter = 0;
        setTimeout(()=>{
            button.disabled = false;
            app.checkWining();
        }, 1000);
    }
}
