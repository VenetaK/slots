import * as PIXI from 'pixi.js';

export default class Renderer {
    public canvas: HTMLCanvasElement;
    public stage: PIXI.WebGLRenderer | PIXI.CanvasRenderer;
    
    constructor (canvas: HTMLCanvasElement, stage: PIXI.WebGLRenderer | PIXI.CanvasRenderer) {
        this.stage = stage;
    }
    
    renderStage () {
        document.body.appendChild(this.stage.view);
    }
    
    renderApp (app) {
        this.stage.render(app);
    }
}