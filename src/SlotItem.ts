import * as PIXI from 'pixi.js';
// import Text from './Text';
export default class SlotItem extends PIXI.Graphics {
    public x: number;
    public y: number;
    public radius: number;
    public text: string;
    constructor(x: number, y: number, radius: number, text: string) {
        super();
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.text = text;
        this.draw();
    }

    draw() {
        this.lineStyle(10, 0xf44253);
        this.beginFill(0x4286f4, 0.5);
        this.drawCircle(110, 110, this.radius);
        this.endFill(); 

        this.addNumber();
    }
    
    isWinning (parentPosition: number) {
        let halfHeight = (this.y - this.radius) - (37 * 200 - 200); 

        if (halfHeight  > 200 && halfHeight < 400) {
            this.clear();
            this.beginFill(0x4286f4, 0.5);
            this.lineStyle(10, 0xf1f442);
            this.drawCircle(110, 110, this.radius);
            this.endFill(); 
        }
    }

    addNumber () {
        var slotNumber = new PIXI.Text(this.text, { font: '100px Snippet', fill: 'white', align: 'center'});
        slotNumber.position.set(110-(slotNumber.width)/2, 110-(slotNumber.height)/2);
        
        this.addChild(slotNumber);
    }
}