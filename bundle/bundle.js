/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var PIXI = __webpack_require__(1);
	var SlotMachine_1 = __webpack_require__(2);
	var SlotColumn_1 = __webpack_require__(6);
	var Renderer_1 = __webpack_require__(5);
	//------------------------------
	var canvas = document.getElementById('canvas');
	var stage = PIXI.autoDetectRenderer(canvas.width, canvas.height, { view: canvas, antialias: true });
	var renderer = new Renderer_1.default(canvas, stage);
	var slotColumn = new SlotColumn_1.default();
	var slotColumn2 = new SlotColumn_1.default();
	var slotColumn3 = new SlotColumn_1.default();
	var slotColumn4 = new SlotColumn_1.default();
	var app = new SlotMachine_1.default([slotColumn, slotColumn2, slotColumn3, slotColumn4]);
	var button = document.getElementById('spin');
	button.onclick = app.onSpin.bind(app);
	app.start(renderer);
	var counter = 0;
	// run the render loop
	animate();
	function animate() {
	    renderer.renderApp(app);
	    requestAnimationFrame(animate);
	    if (app.spin) {
	        button.disabled = true;
	        counter += 1;
	        app.spinSlots();
	        if (counter < 100)
	            return;
	        app.stopSpin();
	        counter = 0;
	        setTimeout(function () {
	            button.disabled = false;
	            app.checkWining();
	        }, 1000);
	    }
	}


/***/ },
/* 1 */
/***/ function(module, exports) {

	module.exports = PIXI;

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var PIXI = __webpack_require__(1);
	var SlotMachine = (function (_super) {
	    __extends(SlotMachine, _super);
	    function SlotMachine(slotColumns) {
	        _super.call(this);
	        this.spin = false;
	        this.slotColumns = slotColumns;
	        this.addChildren();
	    }
	    SlotMachine.prototype.addChildren = function () {
	        for (var i = 0; i < this.slotColumns.length; i++) {
	            this.addChild(this.slotColumns[i]);
	            this.adjustComumnPositions(i);
	            this.slotColumns[i].checkVisible();
	        }
	    };
	    SlotMachine.prototype.onSpin = function () {
	        this.spin = true;
	        for (var i = 0; i < this.children.length; i++) {
	            this.children[i].addItems();
	        }
	    };
	    SlotMachine.prototype.checkWining = function () {
	        for (var i = 0; i < this.children.length; i++) {
	            this.children[i].checkWinning();
	        }
	    };
	    SlotMachine.prototype.stopSpin = function () {
	        this.spin = false;
	    };
	    SlotMachine.prototype.spinSlots = function () {
	        var delay = 0;
	        for (var i = 0; i < this.slotColumns.length; i++) {
	            this.slotColumns[i].onSpin(delay);
	            delay += 300;
	        }
	    };
	    SlotMachine.prototype.adjustComumnPositions = function (i) {
	        var currentItem = this.children[i];
	        var prevItem = this.children[i - 1];
	        this.children[i].x = (i === 0) ? 0 : prevItem.x + prevItem.width + 15;
	        this.children[i].y = -37 * 200;
	    };
	    SlotMachine.prototype.start = function (renderer) {
	        renderer.renderStage();
	    };
	    return SlotMachine;
	}(PIXI.Container));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SlotMachine;


/***/ },
/* 3 */,
/* 4 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var PIXI = __webpack_require__(1);
	// import Text from './Text';
	var SlotItem = (function (_super) {
	    __extends(SlotItem, _super);
	    function SlotItem(x, y, radius, text) {
	        _super.call(this);
	        this.x = x;
	        this.y = y;
	        this.radius = radius;
	        this.text = text;
	        this.draw();
	    }
	    SlotItem.prototype.draw = function () {
	        this.lineStyle(10, 0xf44253);
	        this.beginFill(0x4286f4, 0.5);
	        this.drawCircle(110, 110, this.radius);
	        this.endFill();
	        this.addNumber();
	    };
	    SlotItem.prototype.isWinning = function (parentPosition) {
	        var halfHeight = (this.y - this.radius) - (37 * 200 - 200);
	        if (halfHeight > 200 && halfHeight < 400) {
	            this.clear();
	            this.beginFill(0x4286f4, 0.5);
	            this.lineStyle(10, 0xf1f442);
	            this.drawCircle(110, 110, this.radius);
	            this.endFill();
	        }
	    };
	    SlotItem.prototype.addNumber = function () {
	        var slotNumber = new PIXI.Text(this.text, { font: '100px Snippet', fill: 'white', align: 'center' });
	        slotNumber.position.set(110 - (slotNumber.width) / 2, 110 - (slotNumber.height) / 2);
	        this.addChild(slotNumber);
	    };
	    return SlotItem;
	}(PIXI.Graphics));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SlotItem;


/***/ },
/* 5 */
/***/ function(module, exports) {

	"use strict";
	var Renderer = (function () {
	    function Renderer(canvas, stage) {
	        this.stage = stage;
	    }
	    Renderer.prototype.renderStage = function () {
	        document.body.appendChild(this.stage.view);
	    };
	    Renderer.prototype.renderApp = function (app) {
	        this.stage.render(app);
	    };
	    return Renderer;
	}());
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = Renderer;


/***/ },
/* 6 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var __extends = (this && this.__extends) || function (d, b) {
	    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
	    function __() { this.constructor = d; }
	    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
	};
	var PIXI = __webpack_require__(1);
	var SlotItem_1 = __webpack_require__(4);
	var SlotColumn = (function (_super) {
	    __extends(SlotColumn, _super);
	    function SlotColumn(items) {
	        _super.call(this);
	        this.addItems();
	        ;
	    }
	    SlotColumn.prototype.addItems = function () {
	        this.children = [];
	        for (var i = 0; i < 40; i++) {
	            var number = (Math.floor(Math.random() * 9) + 1).toString();
	            var prevItem = this.children[i - 1];
	            var positionY = (i === 0) ? 0 : prevItem.y + prevItem.radius * 2 + 20;
	            var circle = new SlotItem_1.default(0, positionY, 90, number.toString());
	            circle.visible = false;
	            this.addChild(circle);
	        }
	    };
	    SlotColumn.prototype.checkVisible = function () {
	        for (var i = 0; i < this.children.length; i++) {
	            var currentChild = this.children[i];
	            if (!(currentChild.y - (37 * 200 - 200) < 0)) {
	                currentChild.visible = true;
	            }
	            else {
	                currentChild.visible = false;
	            }
	        }
	    };
	    SlotColumn.prototype.checkWinning = function () {
	        for (var i = 0; i < this.children.length; i++) {
	            this.children[i].isWinning(this.y);
	        }
	    };
	    SlotColumn.prototype.onSpin = function (delay) {
	        var _this = this;
	        if (delay) {
	            setTimeout(function () { _this.spin(); }, delay);
	        }
	        else {
	            this.spin();
	        }
	    };
	    SlotColumn.prototype.spin = function () {
	        for (var i = 0; i < this.children.length; i++) {
	            var circle = this.children[i];
	            this.checkVisible();
	            circle.y += 30;
	        }
	    };
	    return SlotColumn;
	}(PIXI.Container));
	Object.defineProperty(exports, "__esModule", { value: true });
	exports.default = SlotColumn;


/***/ }
/******/ ]);